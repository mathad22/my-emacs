;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;          MATHAD's EMACS CONFIG          ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.

(setq warning-minimum-level :emergency)

(require 'package)
(setq package-enable-at-startup nil)
;; Add GNU and MELPA packages
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
			 ("melpa" . "http://melpa.org/packages/")))
(package-initialize)

;; loading my modules with Cask
(require 'cask "~/.cask/cask.el")
(cask-initialize)

;; adding the PATH to my Emacs configuration
(add-to-list 'load-path "~/my-emacs/core")
(let ((default-directory "~/my-emacs/core"))
  (normal-top-level-add-subdirs-to-load-path))

(require 'main)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(helm-ff-lynx-style-map t t)
 '(menu-bar-mode nil)
 '(package-selected-packages
   (quote
    (vterm zoom xcscope which-key wgrep visual-regexp use-package try telephone-line symon spacegray-theme popwin pdf-tools pandoc multi-term markdown-mode magit hlinum helm-system-packages helm-swoop helm-smex helm-fuzzier helm-flx goto-line-preview google-this flycheck eyebrowse elpy deadgrep cask buffer-move auto-complete all-the-icons-dired)))
 '(safe-local-variable-values (quote ((bug-reference-bug-regexp . "#\\(?2:[0-9]+\\)"))))
 '(tool-bar-mode nil)
 '(tramp-copy-size-limit 10240000))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
