;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;          MATHAD's EMACS CONFIG          ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(menu-bar-mode nil)
 '(safe-local-variable-values (quote ((bug-reference-bug-regexp . "#\\(?2:[0-9]+\\)"))))
 '(tool-bar-mode nil)
 '(tramp-copy-size-limit 10240000 nil (tramp))
 '(zoom-mode t nil (zoom)))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
)

;; split windows vertically
(setq split-height-threshold nil)
(setq split-width-threshold 160)

;; loading my Emacs environment
(add-to-list 'load-path "~/my-emacs/modules/")

;; loading my theme : spacegray and its global config
(load-theme 'spacegray t)
(global-hl-line-mode 1)

(defun highligh-line-number ()
(linum-mode 1)
(defadvice linum-update-window (around linum-dynamic activate)
  (let* ((w (length (number-to-string
                    (count-lines (point-min) (point-max)))))
        (linum-format (concat " %" (number-to-string w) "d ")))
    ad-do-it))
(fringe-mode -1))

(highligh-line-number)

(require 'hlinum)
(hlinum-activate)

;; ;; ask dired to sord directories first
;; (setq dired-listing-switches "-aBhl  --group-directories-first")

;; set Chromium as default browser
(setq browse-url-browser-function 'browse-url-chromium
      browse-url-new-window-flag  t
      browse-url-chromium-new-window-is-tab t)

;; keep `sudo` passwd 1 hour
(require 'em-tramp)
(setq password-cache t)
(setq password-cache-expiry 3600)

;; use 'a' binding in dired mode 
(put 'dired-find-alternate-file 'disabled nil)
;; set helm key left and right navigation
(customize-set-variable 'helm-ff-lynx-style-map t)
;; hide welcome screen
(setq inhibit-startup-message t)
;; show parenthese
(show-paren-mode 1)
;; remove scroll bar
(scroll-bar-mode -1)
;; set multi-term shell
(setq multi-term-program "/bin/bash")
;; change default grep command
(setq grep-command "grep --color -nsrH -e ")

;; my modules configuration
(require 'modules)

;; my keybindings
(require 'keybindings)

(provide 'main)
