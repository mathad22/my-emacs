# My Emacs Modules

## How to install this Emacs configuration

### [Cask](https://cask.readthedocs.io/en/latest/index.html) must be installed :
```bash
	curl -fsSL https://raw.githubusercontent.com/cask/cask/master/go | python
	export PATH="$HOME/.cask/bin:$PATH"
```

### First install the modules :
```bash
    $ cd ~/.emacs.d/
    $ cask install
```

### Then execute these Emacs commands :
```bash
   M-x pdf-tools-install
   M-x all-the-icons-install-fonts
```

## Description of all the modules I use with my Emacs configuration. ##

### Environment ###

[Auto complete](https://github.com/auto-complete/auto-complete) - An intelligent auto-completion extension for Emacs.

[Buffer move](https://github.com/lukhas/buffer-move/blob/master/buffer-move.el) - Easily swap buffers.

[Eye browse](https://github.com/wasamasa/eyebrowse) - a global minor mode for Emacs that allows you to manage your window configurations in a simple manner, just like tiling window managers.

[Helm](https://emacs-helm.github.io/helm/) - An Emacs framework for incremental completions and narrowing selections.

[Popwin](https://github.com/m2ym/popwin-el) - A popup window manager for Emacs which makes you free from the hell of annoying buffers.

[Which key](https://github.com/justbur/emacs-which-key) - A minor mode for Emacs that displays the key bindings following your currently entered incomplete command.

[Zoom](https://github.com/cyrus-and/zoom) - A minor mode takes care of managing the window sizes.

### UI ###

[All the icons](https://github.com/domtronn/all-the-icons.el) - Better icons for Emacs.

[Visual-Regexp](https://github.com/benma/visual-regexp.el) - visual-regexp for Emacs is like replace-regexp, but with live visual feedback directly in the buffer

[Spinner](https://github.com/Malabarba/spinner.el) - Add progress-bars to the mode-line for ongoing operations.

[Symon](https://github.com/zk-phi/symon) - Tiny graphical system monitor.

[Telephone-line](https://github.com/dbordak/telephone-line)  - A new implementation of Powerline for emacs.


### Tools ###

[Cscope](https://github.com/dkogan/xcscope.el) - This is an emacs interface to the [‘cscope’](http://cscope.sf.net) source cross-referencing tool.

[Deadgrep](https://github.com/Wilfred/deadgrep) - Deadgrep is the fast, beautiful text search that your Emacs deserves.

[Google this](https://github.com/Malabarba/emacs-google-this) - It does a google search using the currently selected region, or the expression under point.

[Magit](https://magit.vc/) - An interface to the version control system Git.

[PDF tools](https://github.com/politza/pdf-tools) - A replacement of DocView for PDF files.

[Wgrep](https://github.com/mhayashi1120/Emacs-wgrep) - Allows you to edit a grep buffer and apply those changes to the file buffer.

#### Programming ####

[Elpy](https://elpy.readthedocs.io/en/latest/introduction.html) - An extension for the Emacs text editor to work with Python projects.

[FlyCheck](https://github.com/flycheck/flycheck) - On the fly syntax checking

[Markdown & Pandoc](https://jblevins.org/projects/markdown-mode/) - A major mode for editing Markdown-formatted text.
