;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;          MATHAD's CONFIG EMACS          ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;
;;     MY MODULES     ;;
;;;;;;;;;;;;;;;;;;;;;;;;

(use-package multi-term
  :init (setq multi-term-program "/bin/bash")
  :bind ("C-c C-t" . multi-term)
)

;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'auto-complete-config)
(ac-config-default)

(use-package buffer-move)

(use-package vterm
    :ensure t)

(use-package eyebrowse
  :bind
  ("M-1" . eyebrowse-switch-to-window-config-1)
  ("M-2" . eyebrowse-switch-to-window-config-2)
  ("M-3" . eyebrowse-switch-to-window-config-3)
  ("M-4" . eyebrowse-switch-to-window-config-4)
  ("M-5" . eyebrowse-switch-to-window-config-5)
  :custom
  (eyebrowse-mode t)
  (eyebrowse-switch-back-and-forth t))

(use-package helm-ido-like
  :load-path
  "~/.emacs.d/helm-ido-like.el"
  :commands
  (require helm-ido-like)
  :config
  ([remap execute-extended-command] #'helm-smex)
  :bind
  ("M-X" . helm-smex-major-mode-commands)
  ("C-x r b" . helm-filtered-bookmarks)
  ("C-x C-f" . helm-find-files))

(use-package ido
  :custom
  (ido-mode t))

(use-package popwin
  :custom
  (popwin-mode t))

(use-package which-key
  :custom
  (which-key-mode t))

(use-package zoom
  :custom
  (zoom-mode t))

;;;;;;;;;;;;;;;;
;;     UI     ;;
;;;;;;;;;;;;;;;;

(require 'all-the-icons)
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)

(use-package spinner
  :requires
  spinner)

(use-package symon
  :custom (symon-sparkline-type 'boxed)
  :config (symon-mode))

(use-package telephone-line
  :custom
  (telephone-line-mode 1)
  :init
  (setq telephone-line-primary-left-separator 'telephone-line-cubed-left
        telephone-line-secondary-left-separator 'telephone-line-cubed-hollow-left
        telephone-line-primary-right-separator 'telephone-line-cubed-right
        telephone-line-secondary-right-separator 'telephone-line-cubed-hollow-right)
  (setq telephone-line-height 25
        telephone-line-evil-use-short-tag t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;     TOOLS -- MODULES     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package xcscope
  :commands
  (cscope-setup)
  :requires
  (xcscope))

(use-package deadgrep
  :requires
  deadgrep)

(use-package goto-line-preview
  :bind ("C-c l" . 'goto-line-preview))

(use-package magit
  :requires
  magit)

(use-package pdf-tools
  :load-path "site-lisp/pdf-tools/lisp"
  :magic ("%PDF" . pdf-view-mode)
  :config
  (pdf-tools-install :no-query))

;; (use-package system-packages-use-sudo
;;   :custom
;;   (system-packages-use-sudo t))

(use-package visual-regexp
  :requires
  visual-regexp
  :bind ("C-c r" . 'vr/replace))

(use-package wgrep
  :requires
  wgrep
  :custom
  (wgrep-change-readonly-file t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;     PROGRAMMING -- MODULES     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package elpy
  :config (elpy-enable))

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(use-package markdown-mode
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "pandoc"))

;; (require basic-c-compile)
;; (setq basic-c-compiler "gcc-6"
 ;;     basic-c-compile-all-files nil
 ;;     basic-c-compile-compiler-flags "-Wall -Werror -std=c11"

(provide 'modules)
